package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import java.io.Serializable;

/**
 * Created by Administrador on 14/12/2016.
 */

public class Airport implements Serializable {

    private static final long serialVersionUID = -5710154047605020243L;
    private String iata, lon, iso, status, name, countryName, continent, type, lat, size;

    public Airport(String iata, String lon, String iso, String countryName,  String continent, String name, String status, String type, String lat, String size) {
        this.iata = iata;
        this.lon = lon;
        this.iso = iso;
        this.continent = continent;
        this.countryName = countryName;
        this.name = name;
        this.status = status;
        this.type = type;
        this.lat = lat;
        this.size = size;
    }

    public String getIata() {
        return iata;
    }

    public String getLon() {
        return lon;
    }

    public String getCountryName() {return countryName;}

    public String getIso() {
        return iso;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getContinent() {
        return continent;
    }

    public String getType() {
        return type;
    }

    public String getLat() {
        return lat;
    }

    public String getSize() {
        return size;
    }
}
