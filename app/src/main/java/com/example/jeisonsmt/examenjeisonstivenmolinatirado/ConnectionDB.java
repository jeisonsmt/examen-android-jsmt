package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

/**
 * Created by Administrador on 28/11/2016.
 */

public class ConnectionDB {
    private static String BD_NAME = "proyecto.db";
    private static int VERSION = 1;

    public static SQLiteDatabase getConnectionQuery(Context context) {
        DBHelper dataBase = new DBHelper(context, BD_NAME, null, VERSION);
        SQLiteDatabase db = dataBase.getReadableDatabase();
        return db;
    }

    public static SQLiteDatabase getConnectionWrite(Context context) {
        DBHelper dataBase = new DBHelper(context, BD_NAME, null, VERSION);
        SQLiteDatabase db = dataBase.getWritableDatabase();
        return db;
    }

    public static long insert(Context context, String table, ContentValues values) {
        long result = 0;
        SQLiteDatabase db = getConnectionWrite(context);
        db.beginTransaction();
        try {
            result = db.insert(table, null, values);
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }

    public static int update(Context context, String table, ContentValues value, String where, String[] whereArgs) {
        SQLiteDatabase db = getConnectionWrite(context);
        db.beginTransaction();
        int result = 0;
        try {
            result = db.update(table, value, where, whereArgs);
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }

    public static long delete(Context context, String table, String where, String[] whereArgs){
        long result = 0;
        SQLiteDatabase db = getConnectionWrite(context);
        db.beginTransaction();
        try {
            db.delete(table, where, whereArgs);
            db.setTransactionSuccessful();
        }catch (SQLiteException e){
            e.printStackTrace();
        }finally {
            db.endTransaction();
            db.close();
        }
        return result;
    }

}
