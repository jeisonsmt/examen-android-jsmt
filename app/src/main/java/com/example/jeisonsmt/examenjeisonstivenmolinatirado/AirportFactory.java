package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrador on 14/12/2016.
 */

public class AirportFactory {

    public List<Airport> _getAirports(Context context) {

        String iata, lon, iso, countryName, status, name, continent, type, lat, size;
        final List<Airport> airport = new ArrayList<>();
        SQLiteDatabase db = ConnectionDB.getConnectionQuery(context);

        //Cursor cursor = db.rawQuery("SELECT * FROM Usuarios", null);
        Cursor cursor = db.query("Airports", new String[]{"iata","lon", "iso", "status", "name", "continent",
                        "type", "lat", "size"},
                null, null, null, null, null);

        Log.d("CursorNum", ""+cursor.getCount());
        int columIata = cursor.getColumnIndex("iata");
        int columLon = cursor.getColumnIndex("lon");
        int columIso= cursor.getColumnIndex("iso");
        int columStatus= cursor.getColumnIndex("status");
        int columName= cursor.getColumnIndex("name");
        int columContinent = cursor.getColumnIndex("continent");
        int columType = cursor.getColumnIndex("type");
        int columLat = cursor.getColumnIndex("lat");
        int columSize = cursor.getColumnIndex("size");

        while (cursor.moveToNext()) {
            iata = cursor.getString(columIata);
            lon = cursor.getString(columLon);
            iso = cursor.getString(columIso);
            status = cursor.getString(columStatus);
            name = cursor.getString(columName);
            continent = cursor.getString(columContinent);
            type = cursor.getString(columType);
            lat = cursor.getString(columLat);
            size = cursor.getString(columSize);

            /*SQLiteDatabase db2 = ConnectionDB.getConnectionQuery(context);
            Cursor cursor2 = db2.query("Countries",new String[]{"name","code"},
            "code=?", new String[]{iso}, null, null, null);
            int columCountryName = cursor2.getColumnIndex("name");
            countryName = cursor2.getString(columCountryName);
*/
            airport.add(new Airport(iata, lon, iso, iso, status, name, continent, type, lat, size));
        }
        db.close();

        return airport;
    }


    private List<Airport> _getAirportsFilteredStatus(Context context, String statusFilter) {
        String iata, lon, iso, countryName, status, name, continent, type, lat, size, filterStr;
        final List<Airport> airport = new ArrayList<>();
        if("Open"==statusFilter){filterStr = "1";}else{filterStr = "0";}

        SQLiteDatabase db = ConnectionDB.getConnectionQuery(context);

        //Cursor cursor = db.rawQuery("SELECT * FROM Usuarios", null);
        Cursor cursor = db.query("Airports", new String[]{"iata","lon", "iso", "status", "name", "continent",
                        "type", "lat", "size"},
                "status=?", new String[]{filterStr}, null, null, null);

        Log.d("CursorNum", ""+cursor.getCount());
        int columIata = cursor.getColumnIndex("iata");
        int columLon = cursor.getColumnIndex("lon");
        int columIso= cursor.getColumnIndex("iso");
        int columStatus= cursor.getColumnIndex("status");
        int columName= cursor.getColumnIndex("name");
        int columContinent = cursor.getColumnIndex("continent");
        int columType = cursor.getColumnIndex("type");
        int columLat = cursor.getColumnIndex("lat");
        int columSize = cursor.getColumnIndex("size");

        while (cursor.moveToNext()) {
            iata = cursor.getString(columIata);
            lon = cursor.getString(columLon);
            iso = cursor.getString(columIso);
            status = cursor.getString(columStatus);
            name = cursor.getString(columName);
            continent = cursor.getString(columContinent);
            type = cursor.getString(columType);
            lat = cursor.getString(columLat);
            size = cursor.getString(columSize);

            /*SQLiteDatabase db2 = ConnectionDB.getConnectionQuery(context);
            Cursor cursor2 = db2.query("Countries",new String[]{"name","code"},
            "code=?", new String[]{iso}, null, null, null);
            int columCountryName = cursor2.getColumnIndex("name");
            countryName = cursor2.getString(columCountryName);
*/
            airport.add(new Airport(iata, lon, iso, iso, status, name, continent, type, lat, size));
        }
        db.close();

        return airport;
    }

    private List<Airport> _getAirportsFilteredType(Context context, String typeFilter) {
        String iata, lon, iso, countryName, status, name, continent, type, lat, size, filterStr;
        final List<Airport> airport = new ArrayList<>();


        SQLiteDatabase db = ConnectionDB.getConnectionQuery(context);

        //Cursor cursor = db.rawQuery("SELECT * FROM Usuarios", null);
        Cursor cursor = db.query("Airports", new String[]{"iata","lon", "iso", "status", "name", "continent",
                        "type", "lat", "size"},
                "type=?", new String[]{typeFilter}, null, null, null);

        Log.d("CursorNum", ""+cursor.getCount());
        int columIata = cursor.getColumnIndex("iata");
        int columLon = cursor.getColumnIndex("lon");
        int columIso= cursor.getColumnIndex("iso");
        int columStatus= cursor.getColumnIndex("status");
        int columName= cursor.getColumnIndex("name");
        int columContinent = cursor.getColumnIndex("continent");
        int columType = cursor.getColumnIndex("type");
        int columLat = cursor.getColumnIndex("lat");
        int columSize = cursor.getColumnIndex("size");

        while (cursor.moveToNext()) {
            iata = cursor.getString(columIata);
            lon = cursor.getString(columLon);
            iso = cursor.getString(columIso);
            status = cursor.getString(columStatus);
            name = cursor.getString(columName);
            continent = cursor.getString(columContinent);
            type = cursor.getString(columType);
            lat = cursor.getString(columLat);
            size = cursor.getString(columSize);

            airport.add(new Airport(iata, lon, iso, iso, status, name, continent, type, lat, size));
        }
        db.close();

        return airport;
    }

    public List<Airport> getAirportsFilteredStatus(Context context, String statusFilter) {
        return this._getAirportsFilteredStatus(context, statusFilter);
    }

    public List<Airport> getAirportsFilteredType(Context context, String typeFilter) {
        return this._getAirportsFilteredType(context, typeFilter);
    }



    public List<Airport> getAirports(Context context) {
        return this._getAirports(context);
    }

}
