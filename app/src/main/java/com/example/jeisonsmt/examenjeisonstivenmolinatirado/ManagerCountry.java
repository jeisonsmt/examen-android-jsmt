package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrador on 14/12/2016.
 */

public class ManagerCountry {

    private void _insertCountry(Context context, String jsonStr){
        try {
            JSONArray jsonObj = new JSONArray(jsonStr);
            // looping through All Contacts
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject c = jsonObj.getJSONObject(i);

                String Name = c.getString("name");
                String Code= c.getString("code");

                ContentValues country = new ContentValues();

                // adding each child node to ContentValue
                country.put("name", Name);
                country.put("code", Code);



                // adding contact to contact list
                ConnectionDB.insert(context, "Countries", country);

            }
        } catch (final JSONException e) {
            e.printStackTrace();
        }
    }

    public void insertCountry(Context context, String jsonStr){
        this._insertCountry(context, jsonStr);
    }
}
