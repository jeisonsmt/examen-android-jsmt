package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Administrador on 14/12/2016.
 */

public class AirportAdapter extends ArrayAdapter<Airport> {
    private final List<Airport> airports;
    private Activity context;

    public AirportAdapter(Activity context, List<Airport> airports) {
        super(context, R.layout.list_airports, airports);
        this.context = context;
        this.airports = airports;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View view = inflater.inflate(R.layout.list_airports, null);

        TextView iata = (TextView) view.findViewById(R.id.iataText);
        TextView name = (TextView) view.findViewById(R.id.nombreText);
        TextView country = (TextView) view.findViewById(R.id.paisText);
        TextView tipo = (TextView) view.findViewById(R.id.tipoText);
        ImageView status = (ImageView) view.findViewById(R.id.estadoImg);

        Airport item = airports.get(position);

        iata.setText(item.getIata().toString());
        name.setText(item.getName().toString());
        country.setText(item.getCountryName().toString());
        tipo.setText(item.getType().toString());
        if("closed"==item.getType().toString()){
            Log.d("typeStatus", ""+item.getType().toString());
            status.setImageResource(android.R.drawable.ic_delete);
        }else{
            status.setImageResource(android.R.drawable.ic_input_add);

        }

        return view;
    }
}
