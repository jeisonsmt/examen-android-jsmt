package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrador on 14/12/2016.
 */

public class ManagerAirport {

    private void _insertAirport(Context context, String jsonStr){
        try {
            JSONArray jsonObj = new JSONArray(jsonStr);
            Log.d("jsonCount", ""+jsonObj.length());
            // looping through All Contacts
            for (int i = 0; i < jsonObj.length(); i++) {
                JSONObject c = jsonObj.getJSONObject(i);

                String Iata = c.getString("iata");
                String Lon= c.optString("lon");
                String Iso= c.getString("iso");
                String Status= c.getString("status");
                String Name= c.getString("name");
                String Continent= c.getString("continent");
                String Type = c.getString("type");
                String Lat= c.optString("lat");
                String Size= c.getString("size");

                ContentValues airport = new ContentValues();

                // adding each child node to ContentValue
                airport.put("iata", Iata);
                airport.put("lon", Lon);
                airport.put("iso", Iso);
                airport.put("status", Status);
                airport.put("name", Name);
                airport.put("continent", Continent);
                airport.put("type", Type);
                airport.put("lat", Lat);
                airport.put("size", Size);

                Log.d("Country", airport.toString());
                // adding contact to contact list
                ConnectionDB.insert(context, "Airports", airport);

            }
        } catch (final JSONException e) {
            e.printStackTrace();
        }
    }

    public void insertAirport(Context context, String jsonStr){
        this._insertAirport(context, jsonStr);
    }
}
