package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsAirport extends AppCompatActivity {
    TextView name, type, size, continent, country, iata, status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_airport);

        name = (TextView) findViewById(R.id.airportNameDetails);
        type = (TextView) findViewById(R.id.typeDetails);
        size = (TextView) findViewById(R.id.sizeDetails);
        continent = (TextView) findViewById(R.id.countinentDetails);
        country = (TextView) findViewById(R.id.countryDetails);
        iata = (TextView) findViewById(R.id.iataDetails);
        status = (TextView) findViewById(R.id.statusDetails);

        Airport airport = (Airport) getIntent().getSerializableExtra("airport");

        name.setText(airport.getName());
        type.setText(airport.getType());
        size.setText(airport.getSize());
        continent.setText(airport.getContinent());
        country.setText(airport.getIso());
        iata.setText(airport.getIata());
        status.setText(airport.getStatus());
    }
}
