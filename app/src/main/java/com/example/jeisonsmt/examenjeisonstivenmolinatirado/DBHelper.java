package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrador on 28/11/2016.
 */

public class DBHelper extends SQLiteOpenHelper {
    String sqlAirports = "CREATE TABLE Airports(iata TEXT, lon TEXT, iso TEXT, status TEXT," +
            "name TEXT, continent TEXT, type TEXT, lat TEXT, size TEXT)";


    String sqlCountries = "CREATE TABLE Countries(name TEXT, code TEXT)";



    String[] statements = new String[]{sqlAirports, sqlCountries};
    String[] statementsDrop = new String[]{"Airports", "Countries"};

// then


    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.rawQuery("SELECT * FROM Airports", null);

        } catch (SQLiteException e) {
            for(String sql : statements){
                db.execSQL(sql);
            }
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        for(String sql : statementsDrop){
            db.execSQL("DROP TABLE IF EXISTS "+sql);
        }
        for(String sql : statements){
            db.execSQL(sql);
        }
    }



}
