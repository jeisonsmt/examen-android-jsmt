package com.example.jeisonsmt.examenjeisonstivenmolinatirado;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView lv;
    List<Airport> airport = new ArrayList<>();
    Button bCountry, bStatus, bType, bAll;
    int p = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            }

    @Override
    protected void onResume() {
        super.onResume();

        lv = (ListView) findViewById(R.id.listview);

        bStatus= (Button) findViewById(R.id.filterStatus);
        bType = (Button) findViewById(R.id.filterType);
        bAll = (Button) findViewById(R.id.filterAll);
        final String[] itemsStatus = {"Closed", "Open"};
        bStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Seleccion radio")
                        .setSingleChoiceItems(itemsStatus, p, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int position) {
                                AirportFactory aFactory = new AirportFactory();
                                airport = aFactory.getAirportsFilteredStatus(getApplicationContext(),itemsStatus[position]);
                                setListView();
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();
            }
        });

        bAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AirportFactory aFactory = new AirportFactory();
                airport = aFactory.getAirports(getApplicationContext());
                setListView();
            }
        });
        final String[] itemsType = {"airport", "heliport", "seaplanes"};
        bType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Seleccion radio")
                        .setSingleChoiceItems(itemsType, p, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int position) {
                                AirportFactory aFactory = new AirportFactory();
                                airport = aFactory. getAirportsFilteredType(getApplicationContext(),itemsType[position]);
                                setListView();
                                dialogInterface.dismiss();
                            }
                        });
                builder.create().show();
            }
        });


        String aiports = getJson(R.raw.airports);
        String countries = getJson(R.raw.countries);

        SQLiteDatabase db = ConnectionDB.getConnectionQuery(getApplicationContext());
        Cursor cursor = db.rawQuery("SELECT * FROM Airports", null);

        if (cursor.getCount() == 0){
            ManagerAirport mAirport= new ManagerAirport();
            mAirport.insertAirport(getApplicationContext(), aiports);
            Toast toast = Toast.makeText(getApplicationContext(), "Cargando Datos", Toast.LENGTH_LONG);
            toast.show();
        }

        db = ConnectionDB.getConnectionQuery(getApplicationContext());
        cursor = db.rawQuery("SELECT * FROM Countries", null);
        if (cursor.getCount() == 0){
            ManagerCountry mCountry= new ManagerCountry();
            mCountry.insertCountry(getApplicationContext(), countries);
        }
        db.close();


        AirportFactory aFactory = new AirportFactory();
        airport = aFactory.getAirports(getApplicationContext());
        setListView();

    }

    public void setListView() {
        final AirportAdapter adapterList = new AirportAdapter(MainActivity.this, airport);
        lv.setAdapter(adapterList);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, DetailsAirport.class);
                Bundle mBundle = new Bundle();
                Airport airportSend = (Airport) adapterList.getItem(i);
                mBundle.putSerializable("airport", airportSend);
                intent.putExtras(mBundle);
                startActivity(intent);
            }
        });
    }


    public String getJson(int json){
        InputStream is = getResources().openRawResource(json);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String jsonString = writer.toString();
        return jsonString;
    }
}
